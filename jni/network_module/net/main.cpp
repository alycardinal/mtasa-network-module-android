#include <jni.h>
#include <pthread.h>
#include "net.h"

Client* pClient = 0;

#define LOG_CLIENT_STATS_PATH "/storage/emulated/0/Android/data/com.rockstargames.gtasa/files/MTASA/ClientStats.txt"

void* NetworkThread(void* p)
{
    if(!pClient) 
    {
        __android_log_write(ANDROID_LOG_INFO, "AXL", "Network thread error!");
        pthread_exit(0);
    }

    RakNet::TimeMS endTime = RakNet::GetTimeMS() + 60000 * 5;
	RakNet::TimeMS time = RakNet::GetTimeMS();

	while (time < endTime) 
    {
        pClient->Update(time);

        /** Stats start */
        FILE *fp = 0;
		char text[2048] = { 0 };
        fp = fopen(LOG_CLIENT_STATS_PATH, "wt");

        if(fp)
        {
            RakNetStatistics *rssSender;
		    rssSender = pClient->peer->GetStatistics(pClient->peer->GetSystemAddressFromIndex(0));
		    StatisticsToString(rssSender, text, 3);
		    fprintf(fp,"==== Stats ====\n");
		    fprintf(fp,"%s\n\n", text);
		    fclose(fp);
        }
        /** Stats end */

        time = RakNet::GetTimeMS();
		RakSleep(30);
    }

    pClient->Disconnect();
    delete pClient;
    pClient = 0;
    
    __android_log_write(ANDROID_LOG_INFO, "AXL", "Network thread shutdown..");
    pthread_exit(0);
}

jint JNI_OnLoad(JavaVM *vm, void *reserved)
{
    __android_log_write(ANDROID_LOG_INFO, "AXL", "Initializing Network Module..");

    pClient = new Client();
    pClient->Startup();
    pClient->Connect();

	pthread_t thread;
	pthread_create(&thread, 0, NetworkThread, 0);

	return JNI_VERSION_1_4;
}
