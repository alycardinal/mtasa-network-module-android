#include <stdlib.h> // For atoi
#include <cstring> // For strlen
#include <stdio.h>
#include <android/log.h> // For android log

#include "RakNetSources/Source/RakPeerInterface.h"
#include "RakNetSources/Source/BitStream.h"
#include "RakNetSources/Source/Rand.h"
#include "RakNetSources/Source/RakNetStatistics.h"
#include "RakNetSources/Source/MessageIdentifiers.h"
#include "RakNetSources/Source/Kbhit.h"
#include "RakNetSources/Source/GetTime.h"
#include "RakNetSources/Source/RakAssert.h"
#include "RakNetSources/Source/RakSleep.h"
#include "RakNetSources/Source/Gets.h"

using namespace RakNet;

char *remoteIPAddress = "192.168.1.102";
#define SERVER_PORT 22003

class Client
{
	public:
		Client()
		{
            __android_log_write(ANDROID_LOG_INFO, "AXL", "Initializing client network..");

			peer = RakNet::RakPeerInterface::GetInstance();
		}

		~Client()
		{
            __android_log_write(ANDROID_LOG_INFO, "AXL", "Destroying client network..");

			if(peer) {
				RakNet::RakPeerInterface::DestroyInstance(peer);
			}
		}

		void Startup(void)
		{
			if(!peer) {
				return;
			}

            __android_log_write(ANDROID_LOG_INFO, "AXL", "Startup client network..");

			RakNet::SocketDescriptor socketDescriptor(0, 0);
			socketDescriptor.socketFamily = AF_INET;

			/*
			// ????
				strcpy(socketDescriptor.hostAddress, remoteIPAddress);
				socketDescriptor.port = SERVER_PORT;
			*/

			nextSendTime = 0;

			peer->Startup(65535, &socketDescriptor, 1);
			peer->SetOccasionalPing(true);
			
			RakAssert(b == RAKNET_STARTED);
			isConnected = false;
		}

		void Connect(void)
		{
			if(!peer) {
				return;
			}

            __android_log_write(ANDROID_LOG_INFO, "AXL", "Connecting client network..");
            
			bool b = peer->Connect(remoteIPAddress, (unsigned short) SERVER_PORT, 0, 0, 0) == RakNet::CONNECTION_ATTEMPT_STARTED;

			if (b == false) {
				__android_log_write(ANDROID_LOG_INFO, "AXL", "Client connect call failed!");
			}
		}

		void Disconnect(void)
		{
			if(!peer) {
				return;
			}

            __android_log_write(ANDROID_LOG_INFO, "AXL", "Disconnecting client network..");
			peer->CloseConnection(peer->GetSystemAddressFromIndex(0), true, 0);

			isConnected = false;
		}

		void Update(RakNet::TimeMS curTime)
		{
			if(!peer) {
				return;
			}

			Packet *p = peer->Receive();

			while (p)
			{
				switch (p->data[0])
				{
					case ID_USER_PACKET_ENUM: {
						__android_log_write(ANDROID_LOG_INFO, "AXL", "ID_USER_PACKET_ENUM");
						break;
					}

					case ID_CONNECTION_REQUEST_ACCEPTED: {
						__android_log_write(ANDROID_LOG_INFO, "AXL", "ID_CONNECTION_REQUEST_ACCEPTED");
						isConnected = true;
						break;
					}

					case ID_CONNECTION_ATTEMPT_FAILED: {
						__android_log_write(ANDROID_LOG_INFO, "AXL", "Client Error: ID_CONNECTION_ATTEMPT_FAILED");
						isConnected = false;
						break;
					}

					case ID_ALREADY_CONNECTED: {
						__android_log_write(ANDROID_LOG_INFO, "AXL", "Client Error: ID_ALREADY_CONNECTED");
						break;
					}

					case ID_CONNECTION_BANNED: {
						__android_log_write(ANDROID_LOG_INFO, "AXL", "Client Error: ID_CONNECTION_BANNED");
						break;
					}

					case ID_INVALID_PASSWORD: {
						__android_log_write(ANDROID_LOG_INFO, "AXL", "Client Error: ID_INVALID_PASSWORD");
						break;
					}

					case ID_INCOMPATIBLE_PROTOCOL_VERSION: {
						__android_log_write(ANDROID_LOG_INFO, "AXL", "Client Error: ID_INCOMPATIBLE_PROTOCOL_VERSION");
						break;
					}

					case ID_NO_FREE_INCOMING_CONNECTIONS: {
						__android_log_write(ANDROID_LOG_INFO, "AXL", "Client Error: ID_NO_FREE_INCOMING_CONNECTIONS");
						isConnected = false;
						break;
					}

					case ID_DISCONNECTION_NOTIFICATION: {
						__android_log_write(ANDROID_LOG_INFO, "AXL", "ID_DISCONNECTION_NOTIFICATION");
						isConnected = false;
						break;
					}

					case ID_CONNECTION_LOST: {
						__android_log_write(ANDROID_LOG_INFO, "AXL", "Client Error: ID_CONNECTION_LOST");
						isConnected = false;
						break;
					}
				}

				peer->DeallocatePacket(p);
				p = peer->Receive();
			}
		}

		bool isConnected = false;
		RakPeerInterface *peer = 0;
		RakNet::TimeMS nextSendTime = 0;
};

class CNetBitStream
{
public:
	RakNet::BitStream* bitStream;
	RakNet::BitStream* GetBitStream() { return bitStream; }

public:
	CNetBitStream()
	{
		this->bitStream = new RakNet::BitStream(65536);
	}

	CNetBitStream(RakNet::BitStream * bitStream)
	{
		this->bitStream = bitStream;
	}

	CNetBitStream(unsigned char * data, unsigned int length, bool copy)
	{
		if(data == NULL || data[0] == NULL)
			this->bitStream = new RakNet::BitStream(65536);
		else
			this->bitStream = new RakNet::BitStream(data, length, true);
	}

	int GetReadOffsetAsBits(void)
	{
		bitStream->GetReadOffset();
		return 0;
	}

	void SetReadOffsetAsBits(int iOffset)
	{
		bitStream->SetReadOffset(iOffset);
	}

	void Reset(void)
	{
		bitStream->Reset();
	}

	void ResetReadPointer(void)
	{
		bitStream->ResetReadPointer();
	}

	void Write(const unsigned char & input)
	{
		bitStream->Write(input);
	}

	void Write(const char & input)
	{
		bitStream->Write(input);
	}

	void Write(const unsigned short & input)
	{
		bitStream->Write(input);
	}

	void Write(const short & input)
	{
		bitStream->Write(input);
	}

	void Write(const unsigned int & input)
	{
		bitStream->Write(input);
	}

	void Write(const int & input)
	{
		bitStream->Write(input);
	}

	void Write(const float & input)
	{
		bitStream->Write(input);
	}

	void Write(const double & input)
	{
		bitStream->Write(input);
	}

	void Write(const char * input, int numberOfBytes)
	{
		bitStream->Write(input, numberOfBytes);
	}

	void Write(RakNet::BitStream * syncStruct)
	{
		syncStruct->Write(*this);
	}

	void WriteCompressed(const unsigned char & input)
	{
		bitStream->WriteCompressed(input);
	}

	void WriteCompressed(const char & input)
	{
		bitStream->WriteCompressed(input);
	}

	void WriteCompressed(const unsigned short & input)
	{
		bitStream->WriteCompressed(input);
	}

	void WriteCompressed(const short & input)
	{
		bitStream->WriteCompressed(input);
	}

	void WriteCompressed(const unsigned int & input)
	{
		bitStream->WriteCompressed(input);
	}

	void WriteCompressed(const int & input)
	{
		bitStream->WriteCompressed(input);
	}

	void WriteCompressed(const float & input)
	{
		bitStream->WriteCompressed(input);
	}

	void WriteCompressed(const double & input)
	{
		bitStream->WriteCompressed(input);
	}

	void WriteBits(const char * input, unsigned int numbits)
	{
		bitStream->WriteBits((unsigned char*)input, numbits);
	}

	void WriteBit(bool input)
	{
		if (input)
			bitStream->Write1();
		else
			bitStream->Write0();
	}

	void WriteNormVector(float x, float y, float z)
	{
		bitStream->Write(x);
		bitStream->Write(y);
		bitStream->Write(z);
	}

	void WriteVector(float x, float y, float z)
	{
		bitStream->Write(x);
		bitStream->Write(y);
		bitStream->Write(z);
	}

	void WriteNormQuat(float w, float x, float y, float z)
	{
		bitStream->Write(w);
		bitStream->Write(x);
		bitStream->Write(y);
		bitStream->Write(z);
	}

	void WriteOrthMatrix(float m00, float m01, float m02, float m10, float m11, float m12, float m20, float m21, float m22)
	{
		bitStream->Write(m00);
		bitStream->Write(m01);
		bitStream->Write(m02);
		bitStream->Write(m10);
		bitStream->Write(m11);
		bitStream->Write(m12);
		bitStream->Write(m20);
		bitStream->Write(m21);
		bitStream->Write(m22);
	}

	bool Read(unsigned char & output)
	{
		return bitStream->Read(output);
	}

	bool Read(char & output)
	{
		return bitStream->Read(output);
	}

	bool Read(unsigned short & output)
	{
		return bitStream->Read(output);
	}

	bool Read(short & output)
	{
		return bitStream->Read(output);
	}

	bool Read(unsigned int & output)
	{
		return bitStream->Read(output);
	}

	bool Read(int & output)
	{
		return bitStream->Read(output);
	}

	bool Read(float & output)
	{
		return bitStream->Read(output);
	}

	bool Read(double & output)
	{
		return bitStream->Read(output);
	}

	bool Read(char * output, int numberOfBytes)
	{
		return bitStream->Read(output, numberOfBytes);
	}

	bool Read(RakNet::BitStream * syncStruct)
	{
		syncStruct->Read(*this);
		return true;
	}

	bool ReadCompressed(unsigned char & output)
	{
		return bitStream->ReadCompressed(output);
	}

	bool ReadCompressed(char & output)
	{
		return bitStream->ReadCompressed(output);
	}

	bool ReadCompressed(unsigned short & output)
	{
		return bitStream->ReadCompressed(output);
	}

	bool ReadCompressed(short & output)
	{
		return bitStream->ReadCompressed(output);
	}

	bool ReadCompressed(unsigned int & output)
	{
		return bitStream->ReadCompressed(output);
	}

	bool ReadCompressed(int & output)
	{
		return bitStream->ReadCompressed(output);
	}

	bool ReadCompressed(float & output)
	{
		return bitStream->ReadCompressed(output);
	}

	bool ReadCompressed(double & output)
	{
		return bitStream->ReadCompressed(output);
	}

	bool ReadBits(char * output, unsigned int numbits)
	{
		return bitStream->ReadBits((unsigned char*)output, numbits);
	}

	bool ReadBit()
	{
		return bitStream->ReadBit();
	}

	bool ReadNormVector(float & x, float & y, float & z)
	{
		return false;
	}

	bool ReadVector(float & x, float & y, float & z)
	{
		return false;
	}

	bool ReadNormQuat(float & w, float & x, float & y, float & z)
	{
		return false;
	}

	bool ReadOrthMatrix(float & m00, float & m01, float & m02, float & m10, float & m11, float & m12, float & m20, float & m21, float & m22)
	{
		return false;
	}

	int GetNumberOfBitsUsed(void) const
	{
		return bitStream->GetNumberOfBitsUsed();
	}

	int GetNumberOfBytesUsed(void) const
	{
		return bitStream->GetNumberOfBytesUsed();
	}

	int GetNumberOfUnreadBits(void) const
	{
		return bitStream->GetNumberOfUnreadBits();
	}

	void AlignWriteToByteBoundary(void) const
	{
		bitStream->AlignWriteToByteBoundary();
	}

	void AlignReadToByteBoundary(void) const
	{
		bitStream->AlignReadToByteBoundary();
	}

	unsigned char * GetData(void) const
	{
		return bitStream->GetData();
	}

	unsigned short Version(void) const
	{
		return 0x10;
	}
};
