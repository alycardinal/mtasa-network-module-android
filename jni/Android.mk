LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := network_module
LOCAL_LDLIBS := -llog -ldl -landroid
FILE_LIST += $(wildcard $(LOCAL_PATH)/*/*/*.cpp)
LOCAL_SRC_FILES := $(FILE_LIST:$(LOCAL_PATH)/%=%)
LOCAL_CFLAGS := -w -pthread
LOCAL_CPPFLAGS := -w -pthread
include $(BUILD_SHARED_LIBRARY)